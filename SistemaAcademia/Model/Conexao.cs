﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SistemaAcademia.Model
{
    class Conexao
    {
        static string server = "localhost";
        static string port = "3306";
        static string database = "academia";
        static string user = "root";
        static string password = "";
        static MySqlConnection _conexao;
        public static MySqlConnection conexao
        {
            get
            {
                return _conexao;
            }
        }
        public void Conectar()
        {
            string strConexao;
            strConexao = "server = '"+server+"'; port = "+port+"; database = '"+database+"'; user = '"+user+"'; password = '"+password+ "';SslMode=none";
            _conexao = new MySqlConnection(strConexao);
            _conexao.Open();
        }
        public void Desconectar()
        {
            _conexao.Close();
        }
        public MySqlDataReader Pesquisar(MySqlCommand sql)
        {
            sql.Connection = _conexao;
            MySqlDataReader reader = sql.ExecuteReader();
            return reader;
        }
        public void Executar(string sql)
        {
            Conectar();
            MySqlCommand command = new MySqlCommand(sql, _conexao);
            command.ExecuteNonQuery();
            Desconectar();
        } 
        public MySqlConnection GetConexao()
        {
            return _conexao;
        }
    }
}
