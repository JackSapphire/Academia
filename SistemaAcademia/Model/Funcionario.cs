﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaAcademia.Model
{
    class Funcionario
    {
        private int ID;
        private string Nome;
        private string Fone;
        private string Login;
        private string Senha;
        private bool Adm;

        Funcionario(int ID, string Nome, string Fone, string Login, string Senha, bool Adm)
        {
            this.ID = ID;
            this.Nome = Nome;
            this.Fone = Fone;
            this.Login = Login;
            this.Senha = Senha;
            this.Adm = Adm;
        }

        public void SetID(int ID)
        {
            this.ID = ID;
        }
        public void SetNome(string Nome)
        {
            this.Nome = Nome;
        }
        public void SetFone(string Fone)
        {
            this.Fone = Fone;
        }
        public void SetLogin(string Login)
        {
            this.Login = Login;
        }
        public void SetSenha(string Senha)
        {
            this.Senha = Senha;
        }
        public void SetAdm(bool Adm)
        {
            this.Adm = Adm;
        }

        public int GetID()
        {
            return this.ID;
        }
        public string GetNome()
        {
            return this.Nome;
        }
        public string GetFone()
        {
            return this.Fone;
        }
        public string GetLogin()
        {
            return this.Login;
        }
        public string GetSenha()
        {
            return this.Senha;
        }
        public bool GetAdm()
        {
            return this.Adm;
        }

    }
}
