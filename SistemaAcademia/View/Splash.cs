﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaAcademia.View
{
    public partial class Splash : Form
    {
        Login login = new Login();
        public Splash()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value++;
            } else
            {
                timer1.Enabled = false;
                this.Visible = false;
                login.ShowDialog();
            }
        }
    }
}
