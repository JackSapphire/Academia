﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SistemaAcademia.Controller;
using SistemaAcademia.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaAcademia.View
{
    public partial class Funcionário : Form
    {
        Manipular Cadastrar = new Manipular();
        Pesquisar Pesquisa = new Pesquisar();
        Excluir Excluir = new Excluir();
        public Funcionário()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            DataSet mDataSet = Pesquisa.Cliente(CPesquisa.Text);
            Tabela.ReadOnly = true;
            Tabela.DataSource = mDataSet.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataSet mDataSet = Pesquisa.Cliente(CPesquisa.Text);
            Tabela.ReadOnly = true;
            Tabela.DataSource = mDataSet.Tables[0];
            Tabela.AutoResizeColumns();
    }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ID.Text != "ID")
            {
                Excluir.Cliente(ID.Text);
                MessageBox.Show("Excluido com Sucesso");
                DataSet mDataSet = Pesquisa.Cliente(CPesquisa.Text);
                Tabela.ReadOnly = true;
                Tabela.DataSource = mDataSet.Tables[0];
                Tabela.AutoResizeColumns();
            } else
            {
                MessageBox.Show("Escolha um item da tabela");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (ID.Text != "ID")
            {
                Clientes.SelectedTab = tabPage2;
            } else
            {
                MessageBox.Show("Escolha um item da tabela");
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            LimparCampos();
        }
        private void LimparCampos()
        {
            ID.Text = "";
            Nome.Text = "";
            Login.Text = "";
            Senha.Text = "";
            CSenha.Text = "";
            Fone.Text = "";
            Data.Text = "";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Senha.Text == CSenha.Text)
            {
                Cadastrar.Cliente(ID.Text, Nome.Text, Fone.Text, Login.Text, Senha.Text, Data.Text);
                LimparCampos();
                Clientes.SelectedTab = tabPage1;
                DataSet mDataSet = Pesquisa.Cliente(CPesquisa.Text);
                Tabela.ReadOnly = true;
                Tabela.DataSource = mDataSet.Tables[0];
                Tabela.AutoResizeColumns();

            } else
            {
                MessageBox.Show("As senhas não coincidem");
                Senha.Text = "";
                CSenha.Text = "";
            }
        }

        private void Tabela_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            DataGridViewRow Linha = Tabela.Rows[index];
            ID.Text = Linha.Cells[0].Value.ToString();
            Nome.Text = Linha.Cells[1].Value.ToString();
            Fone.Text = Linha.Cells[2].Value.ToString();
            Login.Text = Linha.Cells[3].Value.ToString();
            Data.Text = Linha.Cells[5].Value.ToString();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void Pesquisar()
        {
            DataSet mDataSet = Pesquisa.Cliente(CPesquisa.Text);
            Tabela.ReadOnly = true;
            Tabela.DataSource = mDataSet.Tables[0];
            Tabela.AutoResizeColumns();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string segundo = DateTime.Now.Second.ToString();
            string minuto = DateTime.Now.Minute.ToString();
            string hora = DateTime.Now.Hour.ToString();
            string dia = DateTime.Now.Day.ToString();
            string mes = DateTime.Now.Month.ToString();
            string ano = DateTime.Now.Year.ToString();
            string caminho = "../../ImagensCadastradas/Cliente_"+Nome.Text+ "_" + dia + "." + mes + "." + ano + " " + hora + "h " + minuto + "min " + segundo + "seg.jpg";

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "jpg files (*jpg)|*.jpg";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string segundo = DateTime.Now.Second.ToString();
            string minuto = DateTime.Now.Minute.ToString();
            string hora = DateTime.Now.Hour.ToString();
            string dia = DateTime.Now.Day.ToString();
            string mes = DateTime.Now.Month.ToString();
            string ano = DateTime.Now.Year.ToString();
            string caminho = "../../PDFs/Lista-Clientes "+dia+"."+mes+"."+ano+" "+hora+"h "+minuto+"min "+segundo+"seg.pdf";
            Document doc = new Document(PageSize.A4);
            var output = new FileStream(caminho, FileMode.Create);
            var writer = PdfWriter.GetInstance(doc, output);

            doc.Open();

            PdfPTable table1 = new PdfPTable(2);
            table1.DefaultCell.Border = 0;
            table1.WidthPercentage = 80;

            PdfPCell cell11 = new PdfPCell();
            cell11.Colspan = 1;
            cell11.AddElement(new Paragraph("ABC Traders Receipt"));
            cell11.AddElement(new Paragraph("Thankyou for shoping at ABC traders,your order details are below"));
            cell11.VerticalAlignment = Element.ALIGN_LEFT;

            PdfPCell cell12 = new PdfPCell();


            cell12.VerticalAlignment = Element.ALIGN_CENTER;


            table1.AddCell(cell11);

            table1.AddCell(cell12);


            PdfPTable table2 = new PdfPTable(3);

            //One row added

            PdfPCell cell21 = new PdfPCell();

            cell21.AddElement(new Paragraph("Photo Type"));

            PdfPCell cell22 = new PdfPCell();

            cell22.AddElement(new Paragraph("No. of Copies"));

            PdfPCell cell23 = new PdfPCell();

            cell23.AddElement(new Paragraph("Amount"));


            table2.AddCell(cell21);

            table2.AddCell(cell22);

            table2.AddCell(cell23);


            //New Row Added

            PdfPCell cell31 = new PdfPCell();

            cell31.AddElement(new Paragraph("Safe"));

            cell31.FixedHeight = 300.0f;

            PdfPCell cell32 = new PdfPCell();

            cell32.AddElement(new Paragraph("2"));

            cell32.FixedHeight = 300.0f;

            PdfPCell cell33 = new PdfPCell();

            cell33.AddElement(new Paragraph("20.00 * " + "2" + " = " + (20 * Convert.ToInt32("2")) + ".00"));

            cell33.FixedHeight = 300.0f;



            table2.AddCell(cell31);

            table2.AddCell(cell32);

            table2.AddCell(cell33);


            PdfPCell cell2A = new PdfPCell(table2);

            cell2A.Colspan = 2;

            table1.AddCell(cell2A);

            PdfPCell cell41 = new PdfPCell();

            cell41.AddElement(new Paragraph("Name : " + "ABC"));

            cell41.AddElement(new Paragraph("Advance : " + "advance"));

            cell41.VerticalAlignment = Element.ALIGN_LEFT;

            PdfPCell cell42 = new PdfPCell();

            cell42.AddElement(new Paragraph("Customer ID : " + "011"));

            cell42.AddElement(new Paragraph("Balance : " + "3993"));

            cell42.VerticalAlignment = Element.ALIGN_RIGHT;


            table1.AddCell(cell41);

            table1.AddCell(cell42);


            doc.Add(table1);

            doc.Close();

            
        }

        private void button12_Click(object sender, EventArgs e)
        {

        }
    }
}
