﻿using SistemaAcademia.Controller;
using SistemaAcademia.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaAcademia
{
    public partial class Login : Form
    {
        Logar root = new Logar();
        Funcionário funcionario = new Funcionário();
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool permission = false;
            if (Tipo.SelectedIndex == 0)
            {
                permission = root.Cliente(Usuario.Text, Senha.Text);
            }
            if (Tipo.SelectedIndex == 1)
            {
                permission = root.Treinador(Usuario.Text, Senha.Text);
            }
            if (Tipo.SelectedIndex == 2)
            {
                permission = root.Funcionário(Usuario.Text, Senha.Text);
            }

            if (permission) {
                MessageBox.Show("Bem Vindo(a)");
                this.Visible = false;
                funcionario.ShowDialog();
            } else
            {
                MessageBox.Show("Dados Incorretos");
                Senha.Text = "";
                Tipo.Text = "";
            }
        }
    }
}
