﻿using MySql.Data.MySqlClient;
using SistemaAcademia.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaAcademia.Controller
{
    class Pesquisar
    {
        Conexao root = null;
        public Pesquisar() {
            root = new Conexao();
        }
        public DataSet Cliente(string Nome)
        {
            root.Conectar();
            string Select = "SELECT * FROM `cliente` WHERE Nome LIKE '%"+Nome+"%'";
            MySqlConnection conexao = root.GetConexao();

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(Select, conexao);
            MySqlCommandBuilder CBuilder = new MySqlCommandBuilder(dataAdapter);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            return ds;
        }
    }
}
