-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Jun-2018 às 17:48
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(200) DEFAULT NULL,
  `Fone` varchar(15) DEFAULT NULL,
  `Login` varchar(50) DEFAULT NULL,
  `Senha` varchar(36) DEFAULT NULL,
  `UltimoPagamento` varchar(8) NOT NULL,
  `treinador_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`ID`, `Nome`, `Fone`, `Login`, `Senha`, `UltimoPagamento`, `treinador_ID`) VALUES
(11, 'asd', '(12) 34567-8654', 'asd', 'asd', '12/34/56', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipamento`
--

CREATE TABLE `equipamento` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(200) DEFAULT NULL,
  `Qnt` int(11) DEFAULT NULL,
  `Disponivel` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `exercicio`
--

CREATE TABLE `exercicio` (
  `cliente_ID` int(11) NOT NULL,
  `equipamento_ID` int(11) NOT NULL,
  `dia` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ficha`
--

CREATE TABLE `ficha` (
  `ID` int(11) NOT NULL,
  `cliente_ID` int(11) NOT NULL,
  `Peso` double DEFAULT NULL,
  `Altura` varchar(45) DEFAULT NULL,
  `BraçoE` double DEFAULT NULL,
  `BraçoD` double DEFAULT NULL,
  `AnteBraçoD` double DEFAULT NULL,
  `AnteBraçoE` double DEFAULT NULL,
  `Peito` double DEFAULT NULL,
  `Abdomen` double DEFAULT NULL,
  `Cintura` double DEFAULT NULL,
  `Quadril` double DEFAULT NULL,
  `CoxaD` double DEFAULT NULL,
  `CoxaE` double DEFAULT NULL,
  `PanturrilhaD` double DEFAULT NULL,
  `PanturrilhaE` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(200) DEFAULT NULL,
  `Fone` varchar(14) DEFAULT NULL,
  `Login` varchar(50) DEFAULT NULL,
  `Senha` varchar(36) DEFAULT NULL,
  `Adm` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`ID`, `Nome`, `Fone`, `Login`, `Senha`, `Adm`) VALUES
(1, 'Administrador', '(85)98765-4321', 'admin', 'admin', 1),
(2, 'NonAdmin', '(85)98765-4321', 'NonAdmin', 'NonAdmin', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `treinador`
--

CREATE TABLE `treinador` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Fone` varchar(14) DEFAULT NULL,
  `Login` varchar(50) DEFAULT NULL,
  `Senha` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `treinador`
--

INSERT INTO `treinador` (`ID`, `Nome`, `Fone`, `Login`, `Senha`) VALUES
(1, 'Teste', 'Teste', 'Teste', 'Teste');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`ID`,`treinador_ID`),
  ADD KEY `fk_cliente_treinador1` (`treinador_ID`);

--
-- Indexes for table `equipamento`
--
ALTER TABLE `equipamento`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `exercicio`
--
ALTER TABLE `exercicio`
  ADD PRIMARY KEY (`cliente_ID`,`equipamento_ID`),
  ADD KEY `fk_cliente_has_equipamento_cliente` (`cliente_ID`),
  ADD KEY `fk_cliente_has_equipamento_equipamento` (`equipamento_ID`);

--
-- Indexes for table `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`ID`,`cliente_ID`),
  ADD KEY `fk_ficha_cliente1` (`cliente_ID`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `treinador`
--
ALTER TABLE `treinador`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `equipamento`
--
ALTER TABLE `equipamento`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ficha`
--
ALTER TABLE `ficha`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `treinador`
--
ALTER TABLE `treinador`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_cliente_treinador1` FOREIGN KEY (`treinador_ID`) REFERENCES `treinador` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `exercicio`
--
ALTER TABLE `exercicio`
  ADD CONSTRAINT `fk_cliente_has_equipamento_cliente` FOREIGN KEY (`cliente_ID`) REFERENCES `cliente` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cliente_has_equipamento_equipamento` FOREIGN KEY (`equipamento_ID`) REFERENCES `equipamento` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `fk_ficha_cliente1` FOREIGN KEY (`cliente_ID`) REFERENCES `cliente` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
